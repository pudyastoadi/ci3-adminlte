<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bbn extends CI_Controller {

    public function __construct() {
        parent::__construct();
	}
	
	public function index()
	{
		$this->template
			->title('Transaksi BBN','BBN Web Application')
			->set_layout('main')
			->build('index');
	}
}
