<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();
	}
	
	public function index()
	{
		$this->template
			->title('Home','BBN Web Application')
			->set_layout('main')
			->build('index');
	}
}
