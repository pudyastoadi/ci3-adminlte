<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title> <?php echo $template['title'];?> </title>
    <!-- Favicon-->
    <link rel="icon" href="<?=base_url('assets/dist/img/kzu-small.png');?>" type="image/png">

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?=base_url('assets/bootstrap/css/bootstrap.min.css');?>">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?=base_url('assets/font-awesome-4.7.0/css/font-awesome.min.css');?>">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?=base_url('assets/ionicons-2.0.1/css/ionicons.min.css');?>">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?=base_url('assets/plugins/iCheck/flat/blue.css');?>">
    <!-- jvectormap -->
    <link rel="stylesheet" href="<?=base_url('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css');?>">
    <!-- Date Picker -->
    <link rel="stylesheet" href="<?=base_url('assets/plugins/datepicker/datepicker3.css');?>">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?=base_url('assets/plugins/daterangepicker/daterangepicker.css');?>">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="<?=base_url('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css');?>">
    
    <!-- jQuery 2.2.3 -->
    <script src="<?=base_url('assets/plugins/jQuery/jquery-2.2.3.min.js');?>"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?=base_url('assets/bootstrap/js/bootstrap.min.js');?>"></script>

    <!-- daterangepicker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
    <script src="<?=base_url('assets/plugins/daterangepicker/daterangepicker.js');?>"></script>
    <!-- datepicker -->
    <script src="<?=base_url('assets/plugins/datepicker/bootstrap-datepicker.js');?>"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="<?=base_url('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js');?>"></script>
    <!-- Slimscroll -->
    <script src="<?=base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js');?>"></script>
    <!-- FastClick -->
    <script src="<?=base_url('assets/plugins/fastclick/fastclick.js');?>"></script>
    
    <!-- DataTables -->
    <link rel="stylesheet" href="<?=base_url('assets/plugins/dtables/dataTables.min.css');?>">
    <script src="<?=base_url('assets/plugins/dtables/dataTables.min.js');?>"></script>
    <link rel="stylesheet" href="<?=base_url('assets/plugins/dtables/buttons.dataTables.min.css');?>"></link>
    <script src="<?=base_url('assets/plugins/dtables/dataTables.buttons.min.js');?>"></script>
    <link rel="stylesheet" href="<?=base_url('assets/plugins/dtables/select.dataTables.min.css');?>"></link>
    <script src="<?=base_url('assets/plugins/dtables/dataTables.select.min.js');?>"></script>
    <script src="<?=base_url('assets/plugins/dtables/fnReloadAjax.js');?>"></script>
    <script src="<?=base_url('assets/plugins/dtables/currency.js');?>"></script>
    
<!--    <link rel="stylesheet" href="<?=base_url('assets/plugins/datatables/dataTables.bootstrap.css');?>">
    <script src="<?=base_url('assets/plugins/datatables/jquery.dataTables.min.js');?>"></script>
    <script src="<?=base_url('assets/plugins/datatables/dataTables.buttons.min.js');?>"></script>
    <script src="<?=base_url('assets/plugins/datatables/dataTables.bootstrap.min.js');?>"></script>-->
    
    <!-- Sweetalert -->
    <link rel="stylesheet" href="<?=base_url('assets/plugins/sweetalert/sweetalert.css');?>">
    <script src="<?=base_url('assets/plugins/sweetalert/sweetalert.min.js');?>"></script>
    
    <!-- Jansy -->
    <link rel="stylesheet" href="<?=base_url('assets/plugins/jasny-bootstrap/css/jasny-bootstrap.min.css');?>">
    <script src="<?=base_url('assets/plugins/jasny-bootstrap/js/jasny-bootstrap.min.js');?>"></script>
    
    <!-- Ajax Form -->
    <script src="<?=base_url('assets/plugins/jquery-form/jquery.form.min.js');?>"></script>
    
    <!-- AdminLTE App -->
    <script src="<?=base_url('assets/dist/js/app.min.js');?>"></script>
    
    <!-- Chosen -->
    <link href="<?=base_url('assets/plugins/chosen/bootstrap-chosen.css');?>" rel="stylesheet">
    <script src="<?=base_url('assets/plugins/chosen/chosen.jquery.js');?>"></script>
    
    <!-- Select2 -->
    <link href="<?=base_url('assets/plugins/select2/select2.css');?>" rel="stylesheet">
    <script src="<?=base_url('assets/plugins/select2/select2.full.min.js');?>"></script>
    
    <!-- Numeral JS -->
    <script src="<?=base_url('assets/plugins/numeral/numeral.min.js');?>"></script>    
    
    <!-- Theme style -->
    <link rel="stylesheet" href="<?=base_url('assets/dist/css/AdminLTE.css');?>">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?=base_url('assets/dist/css/skins/_all-skins.min.css');?>">
    
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js');?>"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js');?>"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-purple sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?=site_url();?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
<span class="logo-mini"><img src="<?=base_url('assets/dist/img/kzu-small.png');?>" style="height: 40px;" /></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">
          BBN Web App
      </span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <li>
                <a href="javascript:void(0);" class="date-time"></a>
            </li>
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="<?=base_url('assets/dist/img/kzu-small.png');?>" class="user-image" alt="User Image">
                <span class="hidden-xs">IT Administrator</span>
              </a>
              <ul class="dropdown-menu">
                <!-- User image -->
                <li class="user-header">
                  <img src="<?=base_url('assets/dist/img/kzu-small.png');?>" class="img-circle" alt="User Image">

                  <p>
                      IT Administrator
                      <small>Karya Zirang Utama</small>
                  </p>
                </li>
                <!-- Menu Body -->
                <li class="user-body">
                  <div class="row">
                    <div class="col-xs-12 text-center">
                        <a href="#">&nbsp;</a>
                    </div>
                  </div>
                  <!-- /.row -->
                </li>
                <!-- Menu Footer-->
                <li class="user-footer">
                  <div class="pull-left">
                   <a href="#" class="btn btn-default btn-flat">Profile</a>
                  </div>
                  <div class="pull-right">
                    <a href="#" class="btn btn-danger btn-flat">Log Out</a>
                  </div>
                </li>
              </ul>
            </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel" style="padding-bottom: 50px;">
        <div class="pull-left info" style="left: 0px;">
          <p>
              IT Administrator
          </p>
          <a href="#" style="font-size: 10px;"><i class="fa fa-map-marker"></i> Semarang, Jawa Tengah</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <?php
            $url = array(
                0 => array(
                    'name' => '<fa class="fa fa-home"></fa> <span>Home</span>',
                    'url' => site_url('home'),
                    'class' => 'home',
                ),
                1=> array(
                    'name' => '<fa class="fa fa-th-large"></fa> <span>BBN</span>',
                    'url' => site_url('bbn'),
                    'class' => 'bbn',
                ),
            );
            
            $class = $this->uri->segment(1);
            foreach ($url as $value) {
                if($class==$value["class"]){
                    echo '<li class="active"><a href="'.$value["url"].'">'.$value["name"].'</a></li>';
                }else{
                    echo '<li class=""><a href="'.$value["url"].'">'.$value["name"].'</a></li>';
                }
            }
        ?>
        
        <li>
            <a href="javascript:void(0);" onclick="logout();" title="Keluar dari aplikasi menuju halaman login">
                <i class="fa fa-sign-out"></i> 
                <span>Keluar</span>
            </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Selamat Datang,
        <small>IT Administrator</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php echo $template['body'];?>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">      
    <div class="pull-right hidden-xs">
        <?php 
            echo  (ENVIRONMENT === 'development') ? 
                ''
                . 'Memory usage : ' 
                . $this->benchmark->memory_usage() 
                . ' / ' 
                . $this->benchmark->elapsed_time() 
                . ' seconds | '
                . 'CodeIgniter Version '
                . CI_VERSION 
                . ' | Engine Ver : ' . phpversion()
                : '' ?>
    </div>
    <div>
        BBN Web Application &copy; 2017 - <?php echo (date('Y'));?>
    </div>
  </footer>
</div>
<!-- ./wrapper -->

<script>
    $(document).ready(function() {

    });
    
    function logout(){
        swal({
            title: "Konfirmasi Keluar Aplikasi !",
            text: "Pilih Ya, jika sudah selesai menggunakan aplikasi",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#c9302c",
            confirmButtonText: "Ya, Keluar!",
            cancelButtonText: "Batalkan!",
            closeOnConfirm: false
        }, function (isConfirm) {
           if(isConfirm){
            swal({
                title: "Keluar Aplikasi",
                text: "Anda Berhasil Keluar Aplikasi",
                type: "success",
            });
           }
        });  
    }
</script>
</body>
</html>
